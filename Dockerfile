FROM debian as build
RUN echo "Hello World" > message

FROM scratch as final
COPY --from=build message .